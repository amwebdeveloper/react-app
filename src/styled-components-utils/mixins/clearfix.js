const clearfix = () => `
  content: "";
  display: block;
  clear: both;
  font-size: 0;
  line-height: 0;
`;

const clearfixH = (lineHeight = 0) => `
  content: "";
  display: block;
  clear: both;
  font-size: 0;
  line-height: ${lineHeight};
`;

export { clearfix, clearfixH };
