/** Importar la librería React **/
import React from 'react';

/** Importar la utilidad storiesOf de storybook **/
import { storiesOf } from '@storybook/react';

/** Importar el componente Paragraph de nuestro fichero index.js **/
import Paragraph from '.';

/** Crear una historia con el componente Paragraph **/
storiesOf('Paragraph', module).add('default', () => <Paragraph />);
