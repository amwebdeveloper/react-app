/** Importar la librer&iacute;a styled-components **/
import styled from 'styled-components';
import { rem } from '../../styled-components-utils/functions';
import { clearfix } from '../../styled-components-utils/mixins';

/** Indicar el estilo para un elemento de tipo p&aacute;rrafo **/
const StyleParagraph = styled.p`
    font-size: 150%;
    font-family: Arial;
    text-align: center;
    background-color: #EFEFEF;
    padding: 2rem;
    max-width: 20rem;
    margin: auto;
    text-transform: lowercase;
    font-variant: small-caps;
    display: flex;
    flex-flow: column;
    align-items: center;
    align-content: flex-start;
    justify-content: flex-start;

    font-size: ${rem('20px')};

    &:after {
        ${clearfix()};
    }
`;

/** Exportar los elementos del fichero **/
export { StyleParagraph };