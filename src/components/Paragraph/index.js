import React from 'react';

/**  styles **/
import {StyleParagraph} from './styled';

/** Crear un componente **/
const Paragraph = () =>  (
    <StyleParagraph>Esto es un párrafo</StyleParagraph>
);

/** Nombrar por defecto para exportar el componente **/
export default Paragraph;
